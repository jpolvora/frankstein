﻿namespace Frankstein.Common
{
    public abstract class UnmanagedDisposable : ManagedDisposable
    {
        ~UnmanagedDisposable()
        {
            Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            //clean natives resources
            DisposeNative();
        }

        protected abstract void DisposeNative();
    }
}