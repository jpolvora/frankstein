using System;
using System.Linq;

namespace Frankstein.Common
{
    public static class ArrayExtensions
    {
        public static T[] Extend<T>(this T[] srcArray, bool insertAfter = true, params T[] addItems) where T : class
        {
            if (addItems == null)
            {
                throw new ArgumentNullException("addItems");
            }
            if (srcArray == null)
            {
                return addItems;
            }

            var result = insertAfter
                ? srcArray.Concat(addItems).ToArray()
                : addItems.Concat(srcArray).ToArray();

            srcArray = result;

            return srcArray;
        }
    }
}