﻿using System;

namespace Frankstein.Common
{
    public abstract class ManagedDisposable : IDisposable
    {
        protected bool Disposed { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Disposed)
                throw new ObjectDisposedException("Object already disposed. Do not call Dispose on a Disposed Object");

            if (disposing)
            {
                //clean managed resources
                DisposeManaged();
            }

            Disposed = true;
        }

        protected abstract void DisposeManaged();
    }
}