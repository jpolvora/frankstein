using System;
using Microsoft.Practices.ServiceLocation;

namespace Frankstein.Common
{
    public class Service
    {
        private static IServiceLocator _locator;

        public static void SetServiceLocator(IServiceLocator locator)
        {
            if (Locator != null)
                throw new InvalidOperationException("Service locator j� foi configurado");
            
            Locator = locator;
        }

        public static IServiceLocator Locator
        {
            get
            {
                if (_locator == null)
                    throw new InvalidOperationException("ServiceLocator n�o foi configurado pela aplica��o.");
                return _locator;
            }
            private set { _locator = value; }
        }
    }
}