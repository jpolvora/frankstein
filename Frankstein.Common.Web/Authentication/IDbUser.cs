using System.Collections.Generic;

namespace Frankstein.Common.Web.Authentication
{
    public interface IDbUser
    {
        List<string> GetRoles();
    }
}