﻿namespace Frankstein.Common.Web.Scheduling
{
    public interface IJob
    {
        void Register();
        void Unregister();
        void Execute();
    }
}