using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Frankstein.Patterns.Repository.EntityFramework
{
    public class DbContextManager : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private bool _disposed;

        public DbContextManager(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _dbContext = context;
        }

        public bool IsDirty
        {
            get { return _dbContext.ChangeTracker.HasChanges(); }
        }

        public void Commit()
        {
            if (_disposed)
                throw new InvalidOperationException("UnitOfWork was disposed! Please start a new unit of work operation");

            if (IsDirty)
                _dbContext.SaveChanges();

        }

        public void Dispose()
        {
            _dbContext.Dispose();

            _disposed = true;
        }

        public T GetByKey<T>(object key) where T : class
        {
            return _dbContext.Set<T>().Find(key);
        }

        public IEnumerable<T> Query<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return _dbContext.Set<T>().Where(predicate).AsNoTracking();
        }

        public IQueryable<T> AsQueryable<T>() where T : class
        {
            return _dbContext.Set<T>();
        }

        public void RegisterNew<T>(T instance) where T : class
        {
            _dbContext.Entry(instance).State = EntityState.Added;
        }

        public void RegisterDirty<T>(T instance) where T : class
        {
            _dbContext.Entry(instance).State = EntityState.Modified;
        }

        public void RegisterDeleted<T>(T instance) where T : class
        {
            _dbContext.Entry(instance).State = EntityState.Deleted;
        }

        public void RegisterClean<T>(T instance) where T : class
        {
            _dbContext.Entry(instance).State = EntityState.Unchanged;
        }
    }
}