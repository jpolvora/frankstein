﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace Frankstein.Patterns.Repository.EntityFramework
{
    public class ObjectContextUnitOfWork : IUnitOfWork
    {
        private readonly Lazy<ObjectContext> _objectContext;
        private bool _disposed;

        protected internal ObjectContext Context
        {
            get { return _objectContext.Value; }
        }

        public ObjectContextUnitOfWork(Lazy<ObjectContext> context)
        {
            _objectContext = context;
        }

        public bool IsDirty
        {
            get
            {
                return Context.ObjectStateManager.GetObjectStateEntries(
                    EntityState.Added |
                    EntityState.Deleted |
                    EntityState.Modified).Any();
            }
        }

        public void Commit()
        {
            if (_disposed)
                throw new InvalidOperationException("UnitOfWork was disposed! Please start a new unit of work operation");

            if (_objectContext.IsValueCreated)
            {
                _objectContext.Value.SaveChanges();
            }
        }

        public void Dispose()
        {
            if (_objectContext.IsValueCreated)
            {
                _objectContext.Value.Dispose();
            }
            _disposed = true;
        }

        public T GetByKey<T>(object key) where T : class
        {
            var entityKey = key as System.Data.Entity.Core.EntityKey;
            if (entityKey == null)
            {

                var set = Context.CreateObjectSet<T>();
                entityKey = Context.CreateEntityKey(set.EntitySet.Name, key);


            }
            return Context.GetObjectByKey(entityKey) as T;
        }

        public IEnumerable<T> Query<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return Context.CreateObjectSet<T>().Where(predicate).AsNoTracking();
        }

        public IQueryable<T> AsQueryable<T>() where T : class
        {
            return Context.CreateObjectSet<T>().AsNoTracking();
        }

        public void RegisterNew<T>(T instance) where T : class
        {
            Context.CreateObjectSet<T>().AddObject(instance);
        }

        public void RegisterDirty<T>(T instance) where T : class
        {
            Context.CreateObjectSet<T>().Attach(instance);
            Context.ObjectStateManager.ChangeObjectState(instance, EntityState.Modified);
        }

        public void RegisterDeleted<T>(T instance) where T : class
        {
            Context.CreateObjectSet<T>().Attach(instance);
            Context.ObjectStateManager.ChangeObjectState(instance, EntityState.Deleted);
        }

        public void RegisterClean<T>(T instance) where T : class
        {
            Context.CreateObjectSet<T>().Attach(instance);
        }
    }
}