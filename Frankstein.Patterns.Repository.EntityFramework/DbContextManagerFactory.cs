using System;
using System.Data.Entity;

namespace Frankstein.Patterns.Repository.EntityFramework
{
    public class DbContextManagerFactory : IUnitOfWorkFactory
    {
        private readonly Func<string, DbContext> _func;

        public DbContextManagerFactory(Func<string, DbContext> func)
        {
            _func = func;
        }

        public IUnitOfWork Create(string serviceName)
        {
            var context = _func(serviceName);

            return new DbContextManager(context);
        }
    }
}