using Frankstein.Patterns.Repository;
using Frankstein.Patterns.Repository.EntityFramework;
using Frankstein.Patterns.Repository.EntityFramework.Tests;
using Frankstein.Storage.DbFileSystem;

namespace ConsoleApplication1
{
    class FileRepository : GenericRepository<DbFile>
    {
        public FileRepository(DbContextUnitOfWork uow)
            : base(uow)
        {
        }
    }

    class File2Repo : GenericRepository<Aluno>
    {
        public File2Repo(ObjectContextUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}