using System;
using System.Data.Entity;
using Frankstein.Patterns.Repository.EntityFramework;
using Frankstein.Storage.DbFileSystem;

namespace ConsoleApplication1
{
    public class DbFileContextUoW : DbContextUnitOfWork
    {
        public DbFileContextUoW()
            : this(new DbFileContext())
        {

        }
        public DbFileContextUoW(DbContext context)
            : base(new Lazy<DbContext>(() => context))
        {
        }
    }
}