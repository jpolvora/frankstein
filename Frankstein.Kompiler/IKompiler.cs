using System.Collections.Generic;

namespace Frankstein.Kompiler
{
    public interface IKompiler
    {
        string CompileFromSource(string assemblyName, Dictionary<string, string> files, out byte[] buffer);
        string CompileFromFolder(string assemblyName, string folder, out byte[] buffer);
        string CompileString(string assemblyName, string text, out byte[] buffer);
    }
}