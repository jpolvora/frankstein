﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Frankstein.Common.Configuration;
using Frankstein.Storage;

namespace Frankstein.Kompiler
{
    public class KompilerStorage
    {
        private readonly IKompiler _kompiler;
        private readonly IStorageProvider _provider;

        public KompilerStorage(IKompiler kompiler, IStorageProvider provider)
        {
            _kompiler = kompiler;
            _provider = provider;
        }

        public string CompileAndSave()
        {
            var src = LoadSourceCodeFromDb();
            byte[] buffer;
            var result = _kompiler.CompileFromSource(BootstrapperSection.Instance.Kompiler.AssemblyName, src, out buffer);

            if (string.IsNullOrEmpty(result))
            {
                SaveCompiledCustomAssembly(buffer);
            }

            return result;
        }

        public Dictionary<string, string> LoadSourceCodeFromDb()
        {
            var dict = new Dictionary<string, string>();

            //procurar por todos os arquivos CS no DbFileSystem
            //using (var ctx = new DbFileContext())
            //{
            //    var csharpFiles = ctx.DbFiles
            //        .Where(x => !x.IsHidden && !x.IsDirectory && x.Extension.Equals(".cs", StringComparison.InvariantCultureIgnoreCase))
            //        .Select(s => new { s.VirtualPath, s.Text })
            //        .ToList();

            //    foreach (var dbFile in csharpFiles)
            //    {
            //        if (string.IsNullOrWhiteSpace(dbFile.Text))
            //            continue;

            //        dict.Add(dbFile.VirtualPath, dbFile.Text);
            //    }
            //}

            return dict;
        }

        public bool ExistsCompiledAssembly()
        {
            //using (var ctx = new DbFileContext())
            //{
            //    return ctx.DbModules.Any(x => x.Name.Equals(KompilerManager.CompiledAssemblyName, StringComparison.InvariantCultureIgnoreCase));
            //}

            return false;
        }

        public void RemoveExistingCompiledAssemblyFromDb()
        {
            //using (var ctx = new DbFileContext())
            //{
            //    var existingFile = ctx.DbModules.FirstOrDefault(x => x.Name.Equals(KompilerManager.CompiledAssemblyName, StringComparison.InvariantCultureIgnoreCase));
            //    if (existingFile != null)
            //    {
            //        ctx.DbModules.Remove(existingFile);
            //        ctx.SaveChanges();
            //        Trace.TraceInformation("[Kompiler]: Compiled Assembly Found and removed.");
            //    }
            //}
        }

        public void SaveCompiledCustomAssembly(byte[] buffer)
        {
            RemoveExistingCompiledAssemblyFromDb();

            //using (var ctx = new DbFileContext())
            //{
            //    var file = new DbModule
            //    {
            //        Name = KompilerManager.CompiledAssemblyName,
            //        CompiledAssembly = buffer,
            //        Status = true
            //    };

            //    ctx.DbModules.Add(file);
            //    ctx.SaveChanges();
            //}
        }
    }
}
