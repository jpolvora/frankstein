﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Frankstein.Common;
using Frankstein.Common.Configuration;
using Frankstein.PluginLoader;
using Frankstein.Storage;

namespace Frankstein.Kompiler
{
    public static class KompilerManager
    {
        public readonly static string CompiledAssemblyName = BootstrapperSection.Instance.Kompiler.AssemblyName;

        private static bool _initialized;

        public static void Execute()
        {
            //todo: usar depdendency injection

            var kompiler = Service.Locator.GetInstance<IKompiler>();
            var storage = Service.Locator.GetInstance<IStorageProvider>();

            var kompilerStorage = new KompilerStorage(kompiler, storage);

            if (_initialized)
                throw new InvalidOperationException("Compilador só pode ser executado no Pre-Start!");

            _initialized = true;


            if (BootstrapperSection.Instance.Kompiler.ForceRecompilation)
            {
                //se forçar a recompilação, remove o assembly existente.
                kompilerStorage.RemoveExistingCompiledAssemblyFromDb();
            }

            var assemblies = PluginManager.Instance.GetAssemblies();
            AddReferences(assemblies.ToArray());

            byte[] buffer = new byte[0];
            string msg;

            try
            {
                if (BootstrapperSection.Instance.Kompiler.LoadFromDb)
                {
                    Trace.TraceInformation("Compiling from DB...");
                    var source = kompilerStorage.LoadSourceCodeFromDb();
                    msg = kompiler.CompileFromSource(CompiledAssemblyName, source, out buffer);
                }
                else
                {
                    var localRootFolder = BootstrapperSection.Instance.DumpToLocal.Folder;
                    Trace.TraceInformation("Compiling from Local File System: {0}", localRootFolder);
                    msg = kompiler.CompileFromFolder(CompiledAssemblyName, localRootFolder, out buffer);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                Trace.TraceInformation("Erro durante a compilação do projeto no banco de dados. \r\n" + ex.Message);
            }

            if (string.IsNullOrWhiteSpace(msg) && buffer.Length > 0)
            {
                Trace.TraceInformation("[Kompiler]: DB Compilation Result: SUCCESS");

                if (!BootstrapperSection.Instance.Kompiler.ForceRecompilation)
                {
                    //só salva no banco se compilação forçada for False
                    kompilerStorage.SaveCompiledCustomAssembly(buffer);
                }


                //
                //throw new NotImplementedException("Salvar assembly!!!");

                //PluginLoaderEntryPoint.SaveAndLoadAssembly(CompiledAssemblyName + ".dll", buffer);
            }
            else
            {
                Trace.TraceInformation("[Kompiler]: DB Compilation Result: Bytes:{0}, Msg:{1}",
                    buffer.Length, msg);
            }
        }


        public static void AddReferences(params Type[] types)
        {
            foreach (var type in types)
            {
                if (!ReferencePaths.Contains(type.Assembly.Location))
                    ReferencePaths.Add(type.Assembly.Location);
            }
        }

        public static void AddReferences(params Assembly[] assemblies)
        {

            foreach (var assembly in assemblies)
            {
                if (!ReferencePaths.Contains(assembly.Location))
                    ReferencePaths.Add(assembly.Location);
            }
        }

        public static void AddReferences(params string[] assemblies)
        {
            foreach (var assembly in assemblies)
            {
                if (!ReferencePaths.Contains(assembly))
                    ReferencePaths.Add(assembly);
            }
        }

        //todo: passar para a classe correta
        public static readonly List<string> ReferencePaths = new List<string>()
        {
                    typeof (object).Assembly.Location, //System
                    typeof (Enumerable).Assembly.Location, //System.Core.dll
                    typeof(System.Data.DbType).Assembly.Location, //System.Data         
                    typeof(Microsoft.CSharp.RuntimeBinder.Binder).Assembly.Location, //Microsoft.CSharp
                    typeof(System.Web.HttpApplication).Assembly.Location,
                    typeof(Trace).Assembly.Location,
                    typeof(System.ComponentModel.DataAnnotations.DataType).Assembly.Location,
                    typeof(System.Xml.Serialization.IXmlSerializable).Assembly.Location,
                    typeof(System.Xml.Linq.XDocument).Assembly.Location,
                    typeof(System.Net.Http.HttpClient).Assembly.Location,
                    typeof(System.Configuration.ConfigurationManager).Assembly.Location
        };
    }
}