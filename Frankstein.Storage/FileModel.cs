﻿using System;

namespace Frankstein.Storage
{
    public class FileModel : IFile
    {
        public int Id { get; set; }
        public bool IsBinary { get; set; }
        public bool IsDirectory { get; set; }
        public byte[] Bytes { get; set; }
        public string Text { get; set; }
        public string VirtualPath { get; set; }
        public string Extension { get; set; }
        public DateTime LastWriteUtc { get; set; }
    }
}