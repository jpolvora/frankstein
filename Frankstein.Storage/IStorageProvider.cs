﻿using System;
using System.Collections.Generic;

namespace Frankstein.Storage
{
    public interface IStorageProvider : IDisposable
    {
        void Init();

        void CreateFile(IFile instance);

        IFile Retrieve(string virtualPath);
        IFile RetrieveById(int id);
        IEnumerable<IFile> RetrieveFiles(Func<IFile, bool> predicate);

        void UpdateFile(IFile instance);
        void DeleteFile(IFile instance);
    }
}
