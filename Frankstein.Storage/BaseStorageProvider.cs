﻿using System;
using System.Collections.Generic;
using System.Transactions;
using Frankstein.Common;

namespace Frankstein.Storage
{
    public abstract class BaseStorageProvider : ManagedDisposable, IStorageProvider, IEnlistmentNotification
    {
        protected bool InTransaction { get; set; }
        protected bool Commited { get; private set; }

        protected BaseStorageProvider()
        {
            if (Transaction.Current != null)
            {
                InTransaction = Transaction.Current.EnlistVolatile(this, EnlistmentOptions.None) != null;
            }
        }

        void IEnlistmentNotification.Prepare(PreparingEnlistment preparingEnlistment)
        {
            preparingEnlistment.Prepared();
        }

        void IEnlistmentNotification.Commit(Enlistment enlistment)
        {
            if (!InTransaction) return;

            Commit();
            Commited = true;
            enlistment.Done();
        }

        void IEnlistmentNotification.Rollback(Enlistment enlistment)
        {
            if (!InTransaction) return;

            Rollback();
            Commited = false;
            enlistment.Done();
        }

        void IEnlistmentNotification.InDoubt(Enlistment enlistment)
        {
            if (!InTransaction) return;

            enlistment.Done();
        }


        ~BaseStorageProvider()
        {
            Dispose(false);
        }

        public virtual void Init()
        {
            CheckDisposed();
        }

        public abstract void Commit();

        public abstract void Rollback();

        protected void CheckDisposed()
        {
            if (Disposed)
                throw new ObjectDisposedException("Objecto já foi disposto!");
        }

        public abstract void CreateFile(IFile instance);

        public abstract IFile Retrieve(string virtualPath);

        public abstract IFile RetrieveById(int id);
        public abstract IEnumerable<IFile> RetrieveFiles(Func<IFile, bool> predicate);

        public abstract void UpdateFile(IFile instance);

        public abstract void DeleteFile(IFile instance);
    }
}