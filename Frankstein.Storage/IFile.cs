﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Frankstein.Patterns.Repository;

namespace Frankstein.Storage
{
    public interface IFile
    {
        int Id { get; }
        bool IsBinary { get; set; }
        bool IsDirectory { get; set; }
        byte[] Bytes { get; set; }
        string Text { get; set; }
        string VirtualPath { get; set; }
        string Extension { get; set; }
        DateTime LastWriteUtc { get; }
    }

    public class FileSystemUnitOfWork: IUnitOfWork
    {
        public T GetByKey<T>(object key) where T : class
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Query<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> AsQueryable<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public void RegisterNew<T>(T instance) where T : class
        {
            throw new NotImplementedException();
        }

        public void RegisterDirty<T>(T instance) where T : class
        {
            throw new NotImplementedException();
        }

        public void RegisterDeleted<T>(T instance) where T : class
        {
            throw new NotImplementedException();
        }

        public void RegisterClean<T>(T instance) where T : class
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool IsDirty { get; private set; }
        public void Commit()
        {
            throw new NotImplementedException();
        }
    }
}