﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Hosting;
using System.Xml.Linq;

namespace Frankstein.PluginLoader
{
    public class PluginManager : Collection<PluginFolder>
    {
        #region static methods

        public static void Initialize()
        {
            var pluginFolders = GetProbingPrivatePath();

            foreach (var directoryInfo in pluginFolders)
            {
                Instance.Add(new PluginFolder(directoryInfo));
            }
            Instance.Load();
        }

        static IEnumerable<DirectoryInfo> GetProbingPrivatePath()
        {
            try
            {
                var configFile = XElement.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

                var probingElement = (from runtime in configFile.Descendants("runtime")
                                      from assemblyBinding in runtime.Elements(XName.Get("assemblyBinding", "urn:schemas-microsoft-com:asm.v1"))
                                      from probing in assemblyBinding.Elements(XName.Get("probing", "urn:schemas-microsoft-com:asm.v1"))
                                      select probing).FirstOrDefault();

                if (probingElement != null)
                {
                    var value = probingElement.Attribute("privatePath").Value;
                    //pode conter vários paths, separados por ';'
                    Trace.TraceInformation("[PluginLoader]:Private Paths are '{0}'", value);
                    var strPaths = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    var directories = strPaths.Select(s =>
                    {
                        var x = s;
                        if (!x.StartsWith("~") || !x.StartsWith("/"))
                        {
                            x = "~/" + x;
                        }
                        return new DirectoryInfo(HostingEnvironment.MapPath(x));

                    });
                    return directories;
                }

            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error reading probing privatePath in web.config. {0}", ex.Message);
            }

            return new DirectoryInfo[0];
        }

        static readonly PluginManager Current;

        static PluginManager()
        {
            Current = new PluginManager();
        }

        public static PluginManager Instance
        {
            get { return Current; }
        }

        #endregion

        #region instance methods

        private PluginManager()
        {
            AppDomain.CurrentDomain.AssemblyLoad += CurrentDomainOnAssemblyLoad;
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
        }

        public void Load()
        {
            foreach (var item in this.Items)
            {
                item.Load();
            }
        }

        private void CurrentDomainOnAssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
            Trace.TraceInformation("Assembly loaded: {0}", args.LoadedAssembly);

            var folder = Path.GetDirectoryName(args.LoadedAssembly.Location);
            if (this.Items.Any(x => x.DirectoryInfo.FullName.Equals(folder, StringComparison.OrdinalIgnoreCase)))
            {
                BuildManager.AddReferencedAssembly(args.LoadedAssembly);
            }

        }

        private Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            return FindAssemblyByName(args.Name);
        }

        private Assembly FindAssemblyByName(string name)
        {
            foreach (var storage in Items)
            {
                foreach (var item in storage)
                {
                    if (item.Key.Equals(name, StringComparison.OrdinalIgnoreCase))
                    {
                        return item.Value;
                    }
                }
            }
            return null;
        }

        #endregion

        public IEnumerable<Assembly> GetAssemblies()
        {
            foreach (var item in Items)
            {
                foreach (var keyValuePair in item)
                {
                    yield return keyValuePair.Value;
                }
            }
        }
    }
}