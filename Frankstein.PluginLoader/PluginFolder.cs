﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Frankstein.PluginLoader
{
    public class PluginFolder : Dictionary<string, Assembly>
    {
        public DirectoryInfo DirectoryInfo { get; private set; }

        public PluginFolder(DirectoryInfo directoryInfo)
        {
            DirectoryInfo = directoryInfo;
        }

        public virtual void Load()
        {
            if (!DirectoryInfo.Exists)
            {
                DirectoryInfo.Create();
            }

            var existingAssemblies = DirectoryInfo.GetFiles("*.dll", SearchOption.AllDirectories);
            var filenames = existingAssemblies.Select(fileInfo => fileInfo.FullName).ToList();

            foreach (var filename in filenames)
            {
                if (this.Any(x => x.Value.Location.Equals(filename, StringComparison.OrdinalIgnoreCase)))
                    continue;

                try
                {
                    Trace.TraceInformation("About to load {0}", filename);
                    var assembly = Assembly.LoadFile(filename);
                    Add(assembly.FullName, assembly);
                }
                catch (Exception ex)
                {
                    Trace.TraceWarning("Erro ao carregar {0}: {1}", filename, ex);
                }
            }
        }
    }
}