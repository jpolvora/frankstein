using System;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;

namespace Frankstein.Patterns.Repository.LinqToSql
{
    public static class DataContextHelpers
    {
        public static T GetByPk<T>(this DataContext context, Table<T> table,  object pk) where T : class
        {
            var metaTable = context.Mapping.GetTable(typeof(T));
            var metaDataMember = metaTable.RowType.DataMembers.SingleOrDefault(d => d.IsPrimaryKey);

            if (metaDataMember == null)
                throw new Exception(String.Format("Table {0} does not contain a Primary Key field", metaTable.TableName));
            var param = Expression.Parameter(typeof(T), "e");
            var predicate = Expression.Lambda<Func<T, bool>>(Expression.Equal(Expression.Property(param, metaDataMember.Name), Expression.Constant(pk)), param);
            return table.SingleOrDefault(predicate);
        }
    }
}