﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;

namespace Frankstein.Patterns.Repository.LinqToSql
{
    public class L2SqlUnitOfWork : IUnitOfWork
    {
        private readonly Lazy<DataContext> _lazyContext;
        private bool _disposed;

        protected internal DataContext DataContext
        {
            get { return _lazyContext.Value; }
        }

        public L2SqlUnitOfWork(Lazy<DataContext> lazyContext)
        {
            if (lazyContext == null)
                throw new ArgumentNullException("lazyContext");

            _lazyContext = lazyContext;
        }

        public virtual T GetByKey<T>(object key) where T : class
        {
            var table = _lazyContext.Value.GetTable<T>();

            return DataContext.GetByPk(table, key);
        }

        public virtual IEnumerable<T> Query<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            var table = DataContext.GetTable<T>();

            return table.Where(predicate).AsEnumerable();
        }

        public virtual IQueryable<T> AsQueryable<T>() where T : class
        {
            var table = DataContext.GetTable<T>();

            return table;
        }

        public void RegisterNew<T>(T instance) where T : class
        {
            DataContext.GetTable<T>().InsertOnSubmit(instance);
        }

        public void RegisterDirty<T>(T instance) where T : class
        {
            DataContext.GetTable<T>().Attach(instance, true);
        }

        public void RegisterDeleted<T>(T instance) where T : class
        {
            DataContext.GetTable<T>().DeleteOnSubmit(instance);
        }

        public void RegisterClean<T>(T instance) where T : class
        {
            DataContext.GetTable<T>().Attach(instance);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            if (_lazyContext.IsValueCreated)
            {
                _lazyContext.Value.Dispose();
                _disposed = true;
            }
        }

        public bool IsDirty
        {
            get
            {
                if (!_lazyContext.IsValueCreated)
                    return false;

                var changeset = DataContext.GetChangeSet();
                return changeset.Inserts.Any() || changeset.Updates.Any() || changeset.Deletes.Any();
            }
        }

        public void Commit()
        {
            if (!_lazyContext.IsValueCreated)
                return;

            if (_disposed)
                throw new InvalidOperationException("UnitOfWork was disposed! Please start a new unit of work operation");


            DataContext.SubmitChanges();
        }
    }
}