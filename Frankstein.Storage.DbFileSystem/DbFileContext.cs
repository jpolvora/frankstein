﻿using System.Data.Entity;
using System.Diagnostics;
using Frankstein.EntityFramework;

namespace Frankstein.Storage.DbFileSystem
{
    public class DbFileContext : DbContextBase
    {
        private static bool _initialized;
        private static readonly object Lock = new object();

        public DbSet<DbFile> DbFiles { get; set; }
        public DbSet<DbModule> DbModules { get; set; }

        public static void Initialize()
        {
            lock (Lock)
            {
                if (_initialized)
                    return;

                _initialized = true;
            }

            using (var db = new DbFileContext())
            {
                Trace.TraceInformation("Initializing DbFileContext: {0}", db.Database.Connection.ConnectionString);
                db.Database.Initialize(false);
            }
        }
    }
}