using System.Data.Entity;

namespace Frankstein.Storage.DbFileSystem
{
    public class DbFileContextMigrationInitializer : MigrateDatabaseToLatestVersion<DbFileContext, DbFileContextMigrationConfiguration>
    {
        
    }
}