﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Frankstein.Storage.DbFileSystem.StorageProvider
{
    public class DbFileSystemStorageProvider : BaseStorageProvider
    {
        private readonly Lazy<DbFileContext> _sharedContext = new Lazy<DbFileContext>();

        //public static void Initialize()
        //{
        //    //var provider = new DbFileSystemStorageProvider();
        //    //var session = provider.CreateSession();
        //    //provider.CreateFile(new DbFile(), session);
        //    //session.SaveChanges();
        //    //using (var scope = new TransactionScope())
        //    //{
        //    //    using (IStorageProvider provider = new DbFileSystemStorageProvider())
        //    //    {
        //    //        //if inside a transaction, will use it.
        //    //        //else, will be an atomic operation, with no rollback possible.
        //    //        provider.CreateFile(new DbFile());

        //    //    }
        //    //    scope.Complete();
        //    //}

        //    DbFileContext.Initialize();
        //}

        private DbFileContext GetContext()
        {
            CheckDisposed();

            var result = InTransaction
               ? _sharedContext.Value
               : new DbFileContext();

            return result;
        }

        private void Save(DbContext ctx)
        {
            if (InTransaction)
                return;

            ctx.SaveChanges();
            ctx.Dispose();
        }

        public override void Init()
        {
            base.Init();

            DbFileContext.Initialize();
        }

        public override void Commit()
        {
            CheckDisposed();

            _sharedContext.Value.SaveChanges();
        }

        public override void Rollback()
        {
            CheckDisposed();
        }

        public override void CreateFile(IFile instance)
        {
            CheckDisposed();

            var context = GetContext();
            context.Entry(instance).State = EntityState.Added;
            Save(context);
        }

        public override IFile Retrieve(string virtualPath)
        {
            CheckDisposed();

            var currentSession = GetContext();

            var dbFile = currentSession.DbFiles.FirstOrDefault(
                    x => x.VirtualPath.Equals(virtualPath, StringComparison.OrdinalIgnoreCase));

            Save(currentSession);
            return dbFile;
        }

        public override IFile RetrieveById(int id)
        {
            CheckDisposed();

            var currentSession = GetContext();

            var dbFile = currentSession.DbFiles.Find(id);

            return dbFile;
        }

        public override IEnumerable<IFile> RetrieveFiles(Func<IFile, bool> predicate)
        {
            CheckDisposed();

            var currentSession = GetContext();

            var files = currentSession.DbFiles.Where(predicate);

            return files;
        }

        public override void UpdateFile(IFile instance)
        {
            CheckDisposed();

            var currentSession = GetContext();
            currentSession.Entry(instance).State = EntityState.Modified;
            Save(currentSession);
        }

        public override void DeleteFile(IFile instance)
        {
            CheckDisposed();

            var currentSession = GetContext();
            currentSession.Entry(instance).State = EntityState.Deleted;
            Save(currentSession);
        }

        protected override void DisposeManaged()
        {
            if (InTransaction && _sharedContext.IsValueCreated)
                _sharedContext.Value.Dispose();
        }
    }
}
