using System.Collections.Generic;

namespace Frankstein.Patterns.Repository
{
    public class PagedResult<TEntity>
    {
        readonly IEnumerable<TEntity> _items;
        readonly int _totalCount;

        public PagedResult(IEnumerable<TEntity> items, int totalCount)
        {
            _items = items;
            _totalCount = totalCount;
        }

        public IEnumerable<TEntity> Items { get { return _items; } }
        public int TotalCount { get { return _totalCount; } }
    }
}