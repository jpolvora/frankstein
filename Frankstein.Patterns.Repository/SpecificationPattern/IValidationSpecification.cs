namespace Frankstein.Patterns.Repository.SpecificationPattern
{
    public interface IValidationSpecification<TEntity> : ISpecification<TEntity>
    {
        string Message { get; }
    }
}