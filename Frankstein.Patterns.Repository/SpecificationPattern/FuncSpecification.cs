using System;

namespace Frankstein.Patterns.Repository.SpecificationPattern
{
    public class FuncSpecification<T> : Specification<T>
    {
        private readonly Func<T, bool> _func;

        public FuncSpecification(Func<T, bool> func)
        {
            _func = func;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return _func(candidate);
        }
    }
}