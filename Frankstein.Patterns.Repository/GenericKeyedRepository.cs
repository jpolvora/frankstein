namespace Frankstein.Patterns.Repository
{
    public class GenericKeyedRepository<TEntity, TKey> : GenericRepository<TEntity>, IKeyedRepository<TEntity, TKey> where TEntity : class, IKeyedEntity<TKey>
    {
        public GenericKeyedRepository(IUnitOfWorkFactory factory, string serviceName)
            : base(factory, serviceName)
        {
        }

        public void InsertOrUpdate(TEntity instance)
        {
            if (instance.Id.Equals(default(TKey)))
            {
                UnitOfWork.RegisterNew(instance);
            }
            else
            {
                UnitOfWork.RegisterDirty(instance);
            }
        }
    }
}