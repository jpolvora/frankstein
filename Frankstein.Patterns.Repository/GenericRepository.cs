using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Frankstein.Patterns.Repository.SpecificationPattern;

namespace Frankstein.Patterns.Repository
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;

        public GenericRepository(IUnitOfWorkFactory factory, string serviceName = "")
        {
            _unitOfWork = factory.Create(serviceName);
        }

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        public virtual T Get(object key)
        {
            return _unitOfWork.GetByKey<T>(key);
        }

        public virtual IEnumerable<T> All()
        {
            return _unitOfWork.AsQueryable<T>();
        }

        public virtual IEnumerable<T> Query(Expression<Func<T, bool>> predicate)
        {
            return _unitOfWork.Query<T>(predicate);
        }

        public virtual IEnumerable<T> Query(ISpecification<T> criteria)
        {
            return _unitOfWork.AsQueryable<T>().Where(criteria.IsSatisfiedBy).ToList();
        }

        public PagedResult<T> PagedQuery(ISpecification<T> filter, int page, int pageSize)
        {
            var pageNumber = page - 1;
            if (pageNumber < 0)
                pageNumber = 0;

            var skip = pageNumber * pageSize;

            var query = _unitOfWork.AsQueryable<T>().Where(filter.IsSatisfiedBy).AsQueryable();

            var count = query.Count();

            var orderedQuery = ApplyOrderBy(query);

            var items = orderedQuery.Skip(skip).Take(pageSize).ToList();
            var result = new PagedResult<T>(items, count);
            return result;
        }

        public void Insert(T instance)
        {
            _unitOfWork.RegisterNew(instance);
        }

        public void Update(T instance)
        {
            _unitOfWork.RegisterDirty(instance);
        }

        public void Delete(T instance)
        {
            _unitOfWork.RegisterDeleted(instance);
        }

        public void Attach(T instance)
        {
            _unitOfWork.RegisterClean(instance);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        protected virtual IOrderedQueryable<T> ApplyOrderBy(IQueryable<T> query)
        {
            return new EnumerableQuery<T>(query);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}