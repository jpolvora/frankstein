namespace Frankstein.Patterns.Repository
{
    public interface IValidator<T> where T : class, new()
    {
        bool CanInsert(T instance, out string message);
        bool CanUpdate(T instance, out string message);
        bool CanDelete(T instance, out string message);
    }
}