namespace Frankstein.Patterns.Repository
{
    public interface IKeyedEntity<T>
    {
        T Id { get; set; }
    }

    public abstract class EntityBase : IKeyedEntity<int>
    {
        public int Id { get; set; }
    }
}