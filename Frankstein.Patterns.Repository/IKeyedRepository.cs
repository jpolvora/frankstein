namespace Frankstein.Patterns.Repository
{
    public interface IKeyedRepository<TEntity, TKey> : IRepository<TEntity>
        where TEntity : class, IKeyedEntity<TKey>
    {
        void InsertOrUpdate(TEntity instance);
    }
}