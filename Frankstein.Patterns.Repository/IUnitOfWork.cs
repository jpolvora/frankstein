using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Frankstein.Patterns.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        void RegisterNew<T>(T instance) where T : class;
        void RegisterDirty<T>(T instance) where T : class;
        void RegisterDeleted<T>(T instance) where T : class;
        void RegisterClean<T>(T instance) where T : class;

        bool IsDirty { get; }
        void Commit();

        T GetByKey<T>(object key) where T : class;
        IEnumerable<T> Query<T>(Expression<Func<T, bool>> predicate) where T : class;
        IQueryable<T> AsQueryable<T>() where T : class;
    }
}