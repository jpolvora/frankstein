using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Frankstein.Patterns.Repository.SpecificationPattern;

namespace Frankstein.Patterns.Repository
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IUnitOfWork UnitOfWork { get; }

        T Get(object key);
        IEnumerable<T> All();
        IEnumerable<T> Query(Expression<Func<T, bool>> predicate);

        IEnumerable<T> Query(ISpecification<T> criteria);
        PagedResult<T> PagedQuery(ISpecification<T> filter, int page, int pageSize);

        //Se for necess�rio utilizar Queryable, � recomend�vel criar um reposit�rio espec�fico
        //IQueryable<T> AsQueryable();

        void Insert(T instance);
        void Update(T instance);
        void Delete(T instance);
        void Attach(T instance);

        void Save();
    }
}