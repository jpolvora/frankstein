using System.Collections.Generic;
using Frankstein.Patterns.Repository.SpecificationPattern;

namespace Frankstein.Patterns.Repository
{
    public class SpecificationValidation<TEntity> : IValidator<TEntity> where TEntity : class, new()
    {
        private readonly ISpecification<TEntity> _specification;

        public SpecificationValidation(ISpecification<TEntity> specification)
        {
            _specification = specification;
        }

        protected IEnumerable<ISpecification<TEntity>> GetInsertSpecifications()
        {
            yield return _specification;
        }

        protected IEnumerable<ISpecification<TEntity>> GetUpdateSpecifications()
        {
            yield return _specification;
        }

        protected IEnumerable<ISpecification<TEntity>> GetDeleteSpecifications()
        {
            yield return _specification;
        }

        public virtual bool CanInsert(TEntity instance, out string message)
        {
            message = "";
            foreach (var specification in GetInsertSpecifications())
            {
                var isValid = specification.IsSatisfiedBy(instance);
                if (!isValid)
                {
                    message = specification.ToString();
                    return false;
                }
            }

            return true;
        }

        public virtual bool CanUpdate(TEntity instance, out string message)
        {
            message = "";
            foreach (var specification in GetUpdateSpecifications())
            {
                var isValid = specification.IsSatisfiedBy(instance);
                if (!isValid)
                {
                    message = specification.ToString();
                    return false;
                }
            }
            return true;
        }

        public virtual bool CanDelete(TEntity instance, out string message)
        {
            message = "";
            foreach (var specification in GetDeleteSpecifications())
            {
                var isValid = specification.IsSatisfiedBy(instance);
                if (!isValid)
                {
                    message = specification.ToString();
                    return false;
                }
            }
            return true;
        }
    }
}