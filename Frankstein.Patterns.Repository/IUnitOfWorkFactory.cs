namespace Frankstein.Patterns.Repository
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(string serviceName);
    }
}