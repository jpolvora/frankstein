﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;

namespace Frankstein.Patterns.Repository.NHibernate
{
    public class NHibernateManager : IUnitOfWork
    {
        private readonly Lazy<ISession> _session;
        private bool _disposed;

        internal ISession Session
        {
            get { return _session.Value; }
        }

        public NHibernateManager(Lazy<ISession> session)
        {
            _session = session;
        }

        public void Dispose()
        {
            if (_session.IsValueCreated)
                _session.Value.Dispose();
            _disposed = true;
        }

        public bool IsDirty
        {
            get { return Session.IsDirty(); }
        }

        public void Commit()
        {
            if (_disposed)
                throw new InvalidOperationException("UnitOfWork was disposed! Please start a new unit of work operation");
            if (_session.IsValueCreated)
                _session.Value.Flush();
        }

        public virtual T GetByKey<T>(object key) where T : class
        {
            return Session.Load<T>(key);
        }


        public virtual IEnumerable<T> Query<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return Session.Query<T>().Where(predicate).AsEnumerable();
        }

        public virtual IQueryable<T> AsQueryable<T>() where T : class
        {
            return Session.Query<T>();
        }

        public virtual void RegisterNew<T>(T instance) where T : class
        {
            Session.Save(instance);
        }

        public virtual void RegisterDirty<T>(T instance) where T : class
        {
            Session.Update(instance);
        }

        public virtual void RegisterDeleted<T>(T instance) where T : class
        {
            Session.Delete(instance);
        }

        public void RegisterClean<T>(T instance) where T : class
        {
            Session.Lock(instance, LockMode.None);
        }
    }
}