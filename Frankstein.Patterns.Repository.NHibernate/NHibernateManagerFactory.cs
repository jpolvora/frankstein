﻿using System;
using NHibernate;

namespace Frankstein.Patterns.Repository.NHibernate
{
    public class NHibernateManagerFactory : IUnitOfWorkFactory
    {
        private readonly Func<string, ISession> _func;

        public NHibernateManagerFactory(Func<string, ISession> func)
        {
            _func = func;
        }

        public IUnitOfWork Create(string serviceName)
        {
            var session = _func(serviceName);
            return new NHibernateManager(new Lazy<ISession>(() => session));
        }
    }
}